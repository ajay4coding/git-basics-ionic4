import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { Platform, ToastController, NavController } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  authState = new BehaviorSubject(false);
  constructor(
    private router: Router,
    private platform: Platform,
    public toastController: ToastController,
    private navCtrl: NavController,
    private storage: Storage,
  ) {
    this.platform.ready().then(() => {
      this.ifLoggedIn();
    });
   }

   ifLoggedIn() {
    this.storage.get('USER_INFO').then((response) => {
      if (response) {
        this.authState.next(true);
      }
    });
  }

  login() {
    console.log("Login clicked")
    let dummy_response = {
      user_id: '007',
      user_name: 'test'
    };
    this.router
      .navigateByUrl('/home1', { replaceUrl: true })
      .then(() => {
        this.storage.set('USER_INFO', dummy_response.toString())
        this.authState.next(true);
      });
      
    // this.storage.setItem('USER_INFO', dummy_response.toString()).then((response) => {
    //   console.log("response",response);
    //   this.router.navigate(['home1'], {replaceUrl: true});
    //   this.navCtrl.navigateRoot('home1');
    //   this.authState.next(true);
    // });
  }

  logout() {
    this.router.navigate(['login'], {replaceUrl: true});
    this.storage.remove('USER_INFO').then(() => {
      this.router.navigateByUrl('/login', {replaceUrl:true});
      this.authState.next(false);
    });
  }

  isAuthenticated() {
    return this.authState.value;
  }

}
