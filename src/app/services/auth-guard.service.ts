import { Injectable } from '@angular/core';
import { CanActivate, CanLoad } from '@angular/router';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate, CanLoad {
  constructor(public authenticationService: AuthenticationService) { }
  canActivate(): boolean{
    return this.authenticationService.isAuthenticated();
  }
  canLoad(): boolean{
    return !this.authenticationService.isAuthenticated();
  }
}
