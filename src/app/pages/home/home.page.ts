import { Router, NavigationExtras } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';
import { SampleModelPage } from 'src/app/model/sample-model/sample-model.page';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  user = {
    name: 'Simon Grimm',
    website: 'www.ionicacademy.com',
    address: {
      zip: 48149,
      city: 'Muenster',
      country: 'DE'
    },
    interests: [
      'Ionic', 'Angular', 'YouTube', 'Sports'
    ]
  };
  constructor(public modalController: PopoverController,
    private authService: AuthenticationService,
    public router: Router) { }

  ngOnInit() {
  }
  async presentModal() {
    const modal = await this.modalController.create({
      component: SampleModelPage,
    componentProps: {
      'firstName': 'Douglas',
      'lastName': 'Adams',
      'middleInitial': 'N'
    },
    translucent: true
    });
    return await modal.present();

    
  }

  logoutUser(){
    this.authService.logout();
  }

  gotoPage(){
    let navigationExtras: NavigationExtras = {
      state: {
        user: this.user
      }
    };
    this.router.navigateByUrl("/home",navigationExtras);
  }
}
