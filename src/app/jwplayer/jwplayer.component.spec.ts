import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JwplayerComponent } from './jwplayer.component';

describe('JwplayerComponent', () => {
  let component: JwplayerComponent;
  let fixture: ComponentFixture<JwplayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JwplayerComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JwplayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
