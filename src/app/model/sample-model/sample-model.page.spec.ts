import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SampleModelPage } from './sample-model.page';

describe('SampleModelPage', () => {
  let component: SampleModelPage;
  let fixture: ComponentFixture<SampleModelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SampleModelPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SampleModelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
