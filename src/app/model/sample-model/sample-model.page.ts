import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
  
@Component({
  selector: 'app-sample-model',
  templateUrl: './sample-model.page.html',
  styleUrls: ['./sample-model.page.scss'],
})
export class SampleModelPage implements OnInit {

  constructor(public modalController: ModalController, navParams: NavParams) {
    console.log(navParams.get('firstName'));
   }

  ngOnInit() {
  }
  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      'dismissed': true
    });
  }
}
